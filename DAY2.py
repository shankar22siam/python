     # list
cars = ['BMW' , 'AUDI', 'BENZ']
cars[1]='FORD'
print(cars)       # mutable
# indexing
print(cars[0:2])
print(cars)
#sorting
cars.sort()
print(cars)
#reversing
cars.reverse()
print(cars)
print('')
            # tuple
my_tuple=('english','tamil','maths','science')
print(my_tuple)
m=("shankar", "nathan")
x,y=m    # swap the values
print(x)
print(my_tuple[2])    # list in tuple
print(my_tuple[-1])
print(len(my_tuple))
             # set
number={0,1,2,0,1,3,4,}
print(number) # eliminate duplicate numbers
number.add(7)
print(number)
            # dictionaries
inventory = {
 "apples" :"430","banas" : '312 ',"pears" : "234"}
print(inventory)
inventory['apple']=30
print(inventory.get("age"))
print(inventory.keys())
print(inventory.values())
     # boolean
x=10
y=15
print("x>y is" , x>y)
print("x<y is" , x<y)
