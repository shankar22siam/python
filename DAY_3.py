  # operators
a = int(input(" Enter the number : "))
if a>0:
  print ( " Number is positive", a)
if a<0:
  print ( " Number is negative", a)
if a==0 :
  print ( " Number is netural", a)
print ( " end ")

# CONTROL FLOW
         # NESTED IF ELSE
x = 103

if x > 10:
  print("Above ten,")
  if x > 20:
    print("and also above 20!")
  else:
    print("but not above 20.")
                # ELIF
                
var = 100
if var == 200:
   print ("1 - Got a true expression value")
   print (var)
elif var == 150:
   print ("2 - Got a true expression value")
   print (var)
elif var == 100:
   print ("3 - Got a true expression value")
   print (var)
else:
   print ("4 - Got a false expression value")
   print (var)

             # FOR LOOP
fruits = ['banana', 'apple',  'mango']
for index in range(len(fruits)):
   print ('Current fruit :', fruits[index])
print ("Good bye!")
